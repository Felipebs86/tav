package localizadorcep;

import br.com.correios.Endereco;

public class CorreioAdapter extends Endereco{

	public CorreioAdapter(long cep) {
		super(formataCEP(cep));
	}
	
	public static String formataCEP(long cep){
		return String.valueOf(cep).substring(0, 5) + "-" + String.valueOf(cep).substring(5, 8);
	}
	
	public String getEnderecoCompleto(){
		return super.getTipoLogradouro() + 
		" " + super.getPrefixoLogradouro() +
		" " + super.getLogradouro() + 
		", " + super.getCidade() +
		", " + super.getEstado();
		
	}
	
}
