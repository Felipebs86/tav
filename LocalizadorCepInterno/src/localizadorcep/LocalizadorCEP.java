package localizadorcep;

/**
 *
 * @author mbelo
 */
public class LocalizadorCEP {
    public String buscarPorCEP(Long cep) {

    	CorreioAdapter ca = new CorreioAdapter(cep);
    	return ca.getEnderecoCompleto();

    }

}
