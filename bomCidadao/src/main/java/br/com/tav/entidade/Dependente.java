package br.com.tav.entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.DurationFieldType;
import org.joda.time.Period;

public abstract class Dependente {

	private String nome;
	private DateTime dataNascimento;
	private BigDecimal valor;

	public Dependente(String nome, DateTime dataNascimento) {
		this.setNome(nome);
		this.setDataNascimento(dataNascimento);
		this.setValor();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		if (nome == null || nome.length() < 3)
			throw new IllegalArgumentException("Nome inv�lido");
		this.nome = nome;
	}

	public DateTime getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(DateTime dataNascimento) {
		if (dataNascimento == null || dataNascimento.isAfterNow())
			throw new IllegalArgumentException("Data nascimento inv�lida");
		this.dataNascimento = dataNascimento;
	}

	public int getIdade() {
		Period period = new Period(dataNascimento, DateTime.now());
		return period.get(DurationFieldType.years());
	}

	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(){
		this.valor = TipoDeDependente.novo(getIdade());
	}

}
