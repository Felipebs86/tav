package br.com.tav.entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class Jovem extends TipoDeDependente {
	
	private static final BigDecimal VALOR = new BigDecimal("22.00");

	@Override
	public BigDecimal getValor() {
		return this.VALOR;
	}

}
