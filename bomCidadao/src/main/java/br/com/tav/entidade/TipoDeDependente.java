package br.com.tav.entidade;

import java.math.BigDecimal;

public abstract class TipoDeDependente {

	abstract public BigDecimal getValor();

	static BigDecimal novo(int idade) {
		BigDecimal valor = BigDecimal.ZERO;
		if (idade < 21) {
			valor  = new Jovem().getValor();
		} else if (idade < 35) {
			valor = new Medio().getValor();
		} else if (idade < 65) {
			valor = new Adulto().getValor();
		} else {
			valor = new Senior().getValor();

		}
		return valor;
	}

}
