package br.com.tav.entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class Senior extends TipoDeDependente {
	
	private static final BigDecimal VALOR = new BigDecimal("320.00");

	@Override
	public BigDecimal getValor() {
		return this.VALOR;
	}

}
