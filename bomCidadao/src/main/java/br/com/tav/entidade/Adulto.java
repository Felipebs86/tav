package br.com.tav.entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class Adulto extends TipoDeDependente {
	
	private static final BigDecimal VALOR = new BigDecimal("150.00");

	@Override
	public BigDecimal getValor() {
		return this.VALOR;
	}

}
