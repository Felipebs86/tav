package br.com.tav.entidade;

import java.math.BigDecimal;

import org.joda.time.DateTime;

public class Medio extends TipoDeDependente {
	
	private BigDecimal VALOR = new BigDecimal("50.00");

	@Override
	public BigDecimal getValor() {
		return this.VALOR;
	}

}
