package br.com.tav.entidade;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;


public class TestaDependente {
	
	@Test
	public void testaNovoDependente(){
		Titular titular = new Titular(1,"Ana");
		Dependente d = new Dependente("Fulano", new DateTime(1986, 8, 14, 00, 00, 00)) {
		};
		titular.adicionaDepentente(d);
		assertEquals(new BigDecimal("50.00"), titular.calcularCustoDependentes());
	}
	
	@Test
	public void testaIdadeDependente(){
		Dependente d = new Dependente("Fulano", new DateTime(1986, 8, 14, 00, 00, 00)) {
		};
		
		assertEquals(30, d.getIdade());
	}
	
	@Test
	public void testaDataNascimento(){
		Dependente d = new Dependente("Fulano", new DateTime(1986, 8, 14, 00, 00, 00)) {
		};
		DateTimeFormatter dataFormatada = DateTimeFormat.forPattern("dd/MM/yyyy");
		
		assertEquals("14/08/1986", d.getDataNascimento().toString(dataFormatada));
	}
}
