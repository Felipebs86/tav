package br.com.bancopog.servico;

import java.sql.SQLException;
import java.util.Calendar;
import java.util.Random;

import br.com.bancopog.dominio.Cliente;
import br.com.bancopog.dominio.Conta;
import br.com.bancopog.persistencia.Dao;

public class Banco<T> {

	private static int geradorDeNumeros = 10000;

	private boolean consultaSerasa(String cpf) {
		// faz uma consulta ao web service do serasa
		return true;
	}

	public Cliente registraCliente(String campoNome, String campoCpf) throws SQLException {
		if (consultaSerasa(campoCpf)) {
			Cliente cliente = new Cliente(campoNome, campoCpf);
			persisteNoBanco(cliente);
			return cliente;
		} else {
			throw new RuntimeException("Não registrado, cliente com pendências no Serasa");
		}
	}

	public int geraNumeroConta() {
		geradorDeNumeros += new Random(42).nextInt(1000);
		return geradorDeNumeros++;
	}

	public Conta registraConta(Cliente titular, int numero) throws SQLException {
		Calendar hoje = Calendar.getInstance();
		Conta novaConta = new Conta(numero, hoje, titular);
		persisteNoBanco(novaConta);
		return novaConta;
	}

	private void persisteNoBanco(Object T) {
		try {
			Dao<T> dao = new Dao<T>();
			dao.adiciona(T);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
